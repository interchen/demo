//
//  UserModel.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/11/17.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit

struct UsersModel: JSONMapper {
    struct User {
        var userId: Int = 0
        var name: String = ""
    }
    
    struct Data {
        var users: [User] = []
    }
    
    var success: Bool = false
    var errMsg: String = ""
    var errCode: Int = 0
    var data: Data = Data()
    
    init?(jsonData: JSON) {
        self.success = jsonData["success"].boolValue
        self.errMsg = jsonData["errMsg"].stringValue
        self.errCode = jsonData["errCode"].intValue
        jsonData["data", "users"].array?.forEach({ (userData) in
            var user = User()
            user.userId = userData["userId"].intValue
            user.name = userData["name"].stringValue
            self.data.users.append(user)
        })
    }
}
