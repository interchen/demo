//
//  BlueLabel.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/12.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class BlueLabel: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        self.textColor = UIColor.blue
    }
}
