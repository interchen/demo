//
//  AsyncDisplayKitViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/6.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class OrderItems: ASDisplayNode {
    
//    convenience override init() {
//        self.init()
//        self.frame = CGRect(x: 0, y: 100, width: 100, height: 200)
//    }
    
    var nameLabel: ASTextNode = {
        var nameLabel = ASTextNode()
        nameLabel.attributedText = NSAttributedString(string: "name")
//        nameLabel.measure(CGSize(width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        return nameLabel
    }()
    
    var addressLabel: ASTextNode = {
        var addressLabel = ASTextNode()
        addressLabel.attributedText = NSAttributedString(string: "addressLabel")
//        addressLabel.measure(CGSize(width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        return addressLabel
    }()

    override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        return ASStackLayoutSpec(direction: .vertical, spacing: 10.0, justifyContent: .center, alignItems: .center, children: [nameLabel, addressLabel])
    }
    
}

class AsyncDisplayKitViewController: ASViewController<ASDisplayNode> {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        self.title = "Async"
        
//        let lableNode = ASTextNode()
//        lableNode.attributedText = NSAttributedString(string: "hehe")
//        lableNode.measure(CGSize(width: self.view.bounds.width, height: CGFloat.greatestFiniteMagnitude))
//        lableNode.frame = CGRect(x: 20, y: 200, width: 100, height: 20)
//        self.view.addSubnode(lableNode)
        
        let container1 = OrderItems()
        container1.frame = CGRect(x: 0, y: 100, width: 100, height: 200)
        self.view.addSubnode(container1)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
