//
//  TestViewController.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/8/24.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit

class TestViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("view controller count = \(self.navigationController?.viewControllers.count ?? 0)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backToHome(_ sender: Any) {
        let _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func backToParent(_ sender: Any) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
