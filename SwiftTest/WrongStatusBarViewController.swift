//
//  WrongStatusBarViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/26.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class WrongStatusBarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "有问题的"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell")
        cell?.textLabel?.text = "\(indexPath.row)"
        return cell!
    }

}
