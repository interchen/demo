//
//  HKButton.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/29.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class HKButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = frame.size.height / 2
        self.layer.masksToBounds = true
        self.backgroundColor = UIColor.red.withAlphaComponent(0.6)
        self.setTitleColor(.white, for: .normal)
    }

}
