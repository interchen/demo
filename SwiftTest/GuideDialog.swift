//
//  GuideDialog.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/11/20.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit

class GuideDialog: UIView {
    
    let contentPadding: CGFloat = 20
    let controlMargin: CGFloat = 20
    
    var fromViewController: UIViewController!
    var fromView: UIView!
    var hintText: String!
    var font: UIFont = UIFont.systemFont(ofSize: 14.0)
    
    var textLabel = UILabel(frame: .zero)
    var button = UIButton(type: .roundedRect)
    
    convenience init(from viewController: UIViewController, for view: UIView, with text: String, font: UIFont) {
        self.init(frame: viewController.view.bounds)
        
        self.fromViewController = viewController
        self.fromView = view
        self.hintText = text
        self.font = font
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        
        self.addSubview(textLabel)
        self.addSubview(button)
        
        button.addTarget(self, action: #selector(handleButtonClick), for: .touchUpInside)
    }
    
    convenience init(from viewController: UIViewController, for view: UIView, with text: String) {
        self.init(from: viewController, for: view, with: text, font: UIFont.systemFont(ofSize: 14.0))
    }

    override func draw(_ rect: CGRect) {
        let maskPath = UIBezierPath(rect: rect)
        let fromViewRectInTopView = self.fromView.superview?.convert(self.fromView.frame, to: self.fromViewController.view) ?? .zero
        let maskFrame = fromViewRectInTopView.insetBy(dx: -10, dy: -10)
        maskPath.append(UIBezierPath(roundedRect: maskFrame, cornerRadius: 2.0).reversing())
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
        
        var textRect = (hintText as NSString).boundingRect(with: CGSize.init(width: rect.size.width - contentPadding*2, height: .greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        textRect = CGRect(x: rect.origin.x + contentPadding, y: maskFrame.origin.y + maskFrame.size.height + controlMargin, width: rect.size.width - contentPadding*2, height: textRect.size.height)
        
        textLabel.frame = textRect
        textLabel.font = self.font
        textLabel.textAlignment = .center
        textLabel.text = self.hintText
        textLabel.textColor = .white
        textLabel.numberOfLines = 0
        
        button.setTitle("Got it", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.contentEdgeInsets = UIEdgeInsetsMake(6, 8, 6, 8)
        button.titleLabel?.font = self.font
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 1.0
        button.layer.cornerRadius = 4.0
        button.layer.masksToBounds = true
        button.sizeToFit()
        button.center = CGPoint(x: textLabel.center.x, y: textLabel.frame.origin.y + textLabel.frame.size.height + controlMargin + button.frame.size.height/2)
    }
    
    @objc func handleButtonClick() -> Void {
        self.removeFromSuperview()
    }
    
    func show() -> Void {
        self.fromViewController.view.addSubview(self)
        self.fromViewController.view.bringSubview(toFront: self)
    }
}
