//
//  JSONMapper.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/11/17.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit

public protocol JSONMapper {
    init?(jsonData: JSON)
}
