//
//  ICZoomableImageView.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/29.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

@objc protocol ICZoomableImageViewDelegate {
    func zoomableImageView(_ icZoomableImageView: ICZoomableImageView, load url: URL, into imageView: UIImageView)
    
    @objc optional func zoomableImageViewDidTap(_ icZoomableImageView: ICZoomableImageView)
    @objc optional func zoomableImageViewDidDoubleTap(_ icZoomableImageView: ICZoomableImageView)
    @objc optional func zoomableImageViewDidLongPress(_ icZoomableImageView: ICZoomableImageView)
}

enum ImageWidthMode: Int {
    case fill, original
}

class ICZoomableImageView: UIScrollView, UIScrollViewDelegate {
    
    /// The view to show image
    open var imageView = ICImageView()
    
    /// The url to load image. You can pass a remote or a local URL
    open var url: URL?
    
    open var imageWidthMode: ImageWidthMode = .fill
    
    /// The delegate to load image
    open weak var imageLoader: ICZoomableImageViewDelegate?
    
    // private
    fileprivate let defaultMaximumZoomScale: CGFloat = 3.0
    
    override var contentMode: UIViewContentMode {
        get {
            return self.imageView.contentMode
        }
        
        set {
            self.imageView.contentMode = newValue
        }
    }
    
    deinit {
        self.removeObserver(self, forKeyPath: "bounds")
        self.imageLoader = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let keyPath = keyPath, keyPath == "bounds" else {
            return
        }
        
        // center subview
        self.imageView.center = self.superview?.convert(self.center, to: self) ?? .zero
    }
    
    fileprivate func setup() {
        self.addObserver(self, forKeyPath: "bounds", options: .new, context: nil)
        
        // imageView
        self.imageView.isUserInteractionEnabled = true
        let initWidth = self.frame.size.width / 2
        self.imageView.frame = CGRect(origin: .zero, size: CGSize(width: initWidth, height: initWidth))
        self.addSubview(imageView)
        
        self.delegate = self
        self.maximumZoomScale = defaultMaximumZoomScale
        self.minimumZoomScale = 1.0
//        self.contentOffset = .zero
        
        // gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        tapGesture.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapGesture)
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTap(recognizer:)))
        doubleTapGesture.numberOfTapsRequired = 2
        self.imageView.addGestureRecognizer(doubleTapGesture)
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(recognizer:)))
        self.addGestureRecognizer(longPressGesture)
        
        tapGesture.require(toFail: doubleTapGesture)
    }
    
    open func load(_ url: URL, with imageLoader: ICZoomableImageViewDelegate) {
        self.url = url
        self.imageLoader = imageLoader
        self.imageLoader?.zoomableImageView(self, load: url, into: self.imageView)
    }
    
    // MARK: - Actions
    @objc func handleTap(recognizer: UIGestureRecognizer) {
        
    }
    
    @objc func handleDoubleTap(recognizer: UIGestureRecognizer) {
        let location = recognizer.location(in: self.imageView)
        let center = self.convert(location, to: self)
        
        if self.zoomScale == self.maximumZoomScale {
            // Zoom out
            self.minimumZoomScale = 1.0
            self.setZoomScale(self.minimumZoomScale, animated: true)
        } else {
            // Zoom in
            self.minimumZoomScale = 1.0
            let rect = self.zoomRectForScrollView(self, withScale: self.maximumZoomScale, withCenter: center)
            self.zoom(to: rect, animated: true)
        }
    }
    
    @objc func handleLongPress(recognizer: UIGestureRecognizer) {
        
    }
    
    // MARK: - UIScrollViewDelegate for zoom
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let y = max(0, (scrollView.frame.size.height - self.imageView.frame.height) / 2)
        let x = max(0, (scrollView.frame.size.width - self.imageView.frame.width) / 2)
        
        self.imageView.frame.origin = CGPoint(x: x, y: y)
//        if self.zoomScale == 1.0 {
//            self.contentOffset = .zero
//        }
    }
    
    fileprivate func zoomRectForScrollView(_ scrollView: UIScrollView, withScale scale: CGFloat, withCenter center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        
        // The zoom rect is in the content view's coordinates.
        // At a zoom scale of 1.0, it would be the size of the
        // imageScrollView's bounds.
        // As the zoom scale decreases, so more content is visible,
        // the size of the rect grows.
        zoomRect.size.height = scrollView.frame.size.height / scale
        zoomRect.size.width  = scrollView.frame.size.width  / scale
        
        // choose an origin so as to get the right center.
        zoomRect.origin.x = center.x - (zoomRect.size.width  / 2.0)
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0)
        
        return zoomRect
    }

}

class ICImageView: UIImageView {
    override var image: UIImage? {
        didSet {
            if let zoomableImageView = self.superview as? ICZoomableImageView {
                guard image != nil else {
                    return
                }
                
                let imageWidth = (CGFloat) (image?.cgImage?.width ?? 0)
                let imageHeight = (CGFloat) (image?.cgImage?.height ?? 0)
                
                var newWidth: CGFloat = 0.0
                switch zoomableImageView.imageWidthMode {
                case .fill:
                    newWidth = zoomableImageView.frame.size.width
                default:
                    newWidth = imageWidth < zoomableImageView.frame.size.width ? imageWidth : zoomableImageView.frame.size.width
                }
                let newHeight = newWidth * imageHeight / imageWidth
                let imageSize = CGSize(width: newWidth, height: newHeight)
                self.frame = CGRect(origin: CGPoint.zero, size: imageSize)
                
                zoomableImageView.contentSize = imageSize
                
                if newHeight < zoomableImageView.frame.size.height {
                    let superViewCenter: CGPoint = zoomableImageView.superview?.convert(zoomableImageView.center, to: zoomableImageView) ?? .zero
                    self.center = superViewCenter
                }
            }
        }
    }
}
