//
//  LaunchViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 16/8/15.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.imageView.image = UIImage(data: try! Data(contentsOf: URL(string: "http://ww4.sinaimg.cn/large/006tKfTcgw1f6uge87j9hj30u01hctfl.jpg")!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
