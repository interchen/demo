//
//  UsersViewModel.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/11/17.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit

protocol UsersDelegate: class {
    func handleError(_ error: Error?)
}

class UsersViewModel: NSObject {
    var users: Variable<[UsersModel.User]> = Variable([])
    var isLoading: Variable<Bool> = Variable(false)
    weak var delegate: UsersDelegate?
    
    private var pageIndex = 0
    private let pageSize = 10
    
    func fetchData() -> Void {
        users.value.removeAll()
        pageIndex = 0
        fetchData(pageIndex, pageSize: pageSize)
    }
    
    func loadMore() -> Void {
        pageIndex += 1
        fetchData(pageIndex, pageSize: pageSize)
    }
    
    private func fetchData(_ pageIndex: Int, pageSize: Int) -> Void {
        isLoading.value = true
        
        Alamofire.request("http://rap.mobile.hkt.com/mockjsdata/24/users")
            .responseJSON { (response) in
                if let value = response.result.value {
                    if let usersModel = UsersModel(jsonData: JSON(value)) {
                        self.users.value.append(contentsOf: usersModel.data.users)
                    }
                } else {
                    self.delegate?.handleError(response.result.error)
                }
                
                self.isLoading.value = false
        }
    }
}
