//
//  UsersViewController.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/11/17.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit
import MBProgressHUD

class UsersViewController: UITableViewController {
    
    var usersViewModel: UsersViewModel = UsersViewModel()
    var disponsed = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = nil
        self.tableView.dataSource = nil
        
        usersViewModel.delegate = self

        usersViewModel.users.asObservable().bind(to: tableView.rx.items(cellIdentifier: "userItem", cellType: UserTableViewCell.self)) {
            (row, element, cell) in
            cell.config(element)
        }.disposed(by: self.disponsed)
        
        usersViewModel.isLoading.asObservable().bind { (isLoading) in
            if isLoading {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            } else {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }.disposed(by: self.disponsed)
        
        usersViewModel.fetchData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func handleRefresh(_ sender: Any) {
        usersViewModel.fetchData()
    }
    
    @IBAction func handleLoadMore(_ sender: Any) {
        usersViewModel.loadMore()
    }
}

extension UsersViewController: UsersDelegate {
    func handleError(_ error: Error?) {
        print(error.debugDescription)
    }
}
