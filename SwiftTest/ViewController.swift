//
//  ViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 15/12/15.
//  Copyright © 2015年 陈 颜俊. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let alertController = UIAlertController(title: "title", message: "message", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: { (action) in
            print("cancel")
        }))
        alertController.addAction(UIAlertAction(title: "ok", style: .default, handler: { (action) in
            print("OK")
        }))
        self.present(alertController, animated: true, completion: nil)
        
//        let alertView = UIAlertView (title: "title", message: "message", delegate: nil, cancelButtonTitle: "cancel", otherButtonTitles: "ok")
//        alertView.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath)
        switch (indexPath as NSIndexPath).row {
        case 0:
            cell.contentView.backgroundColor = UIColor.black
        case 1:
            cell.contentView.backgroundColor = UIColor.red
        case 2:
            cell.contentView.backgroundColor = UIColor.blue
        case 3:
            cell.contentView.backgroundColor = UIColor.yellow
//        case 4:
//            cell.contentView.backgroundColor = UIColor.greenColor()
        default:
            cell.contentView.backgroundColor = UIColor.gray
        }
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch (indexPath as NSIndexPath).row {
        case 0:
            return CGSize.init(width: 150, height: 250)
        case 1:
            return CGSize.init(width: 150, height: 100)
        case 2:
            return CGSize.init(width: 60, height: 100)
        case 3:
            return CGSize.init(width: 60, height: 100)
//        case 4:
//            return CGSize.init(width: 200, height: 100)
        default:
            return CGSize.init(width: 160, height: 10)
        }
    }
}

