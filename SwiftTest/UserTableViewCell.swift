//
//  UserTableViewCell.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/11/17.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(_ user: UsersModel.User) {
        self.textLabel?.text = "userId: " + String(user.userId)
        self.detailTextLabel?.text = user.name
    }

}
