//
//  NavigationBarTestViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/23.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class NavigationBarTestViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var originNavBgColor: UIColor?
    var originNavHidden: Bool?
    
    let navBgImage = #imageLiteral(resourceName: "nav-bg-default")
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setNeedsStatusBarAppearanceUpdate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.originNavBgColor = self.navigationController?.zj_alphaView?.backgroundColor
//        self.originNavHidden = self.navigationController?.isNavigationBarHidden
//        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.navigationController?.zj_alphaView?.backgroundColor = self.originNavBgColor
//        self.navigationController?.setNavigationBarHidden(self.originNavHidden ?? false, animated: true)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell")
        cell?.textLabel?.text = "\(indexPath.row)"
        return cell!
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if scrollView.contentOffset.y + 64.0 > 0.0 {
            var alpha = (scrollView.contentOffset.y + 64.0) / 64.0
            print("offset y = \(scrollView.contentOffset.y), alpha = \(alpha)")
            if alpha > 0.99 {
                alpha = 0.99
            }
            
            self.navigationController?.zj_updateWithAlpha(alpha: alpha)
//            let alphaNavBGImage = self.navBgImage.imageWithAlpha(alpha)
//            self.navigationController?.navigationBar.setBackgroundImage(alphaNavBGImage, for: .default)
            self.navigationController?.navigationBar.shadowImage = nil
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        } else {
            self.navigationController?.zj_updateWithAlpha(alpha: 0)
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
//            self.navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    

}

extension UIImage {
    class func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    func imageWithAlpha(_ alpha: CGFloat) -> UIImage {
        let colorWithAlpha = UIColor(patternImage: self).withAlphaComponent(alpha)
        return UIImage.imageWithColor(colorWithAlpha)
    }
}

extension UIColor {
    
    /**
     将16进制的RGB或ARGB转换成UIColor对象
     
     - parameter hex: #AARRGGBB 或 #RRGGBB 或 AARRGGBB 或 RRGGBB
     
     - returns: UIColor对象
     */
    public static func color(withHex hex: String) -> (UIColor) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        guard cString.characters.count == 6 || cString.characters.count == 8 else {
            return UIColor.white
        }
        
        var aString = "1"
        var rString = "0"
        var gString = "0"
        var bString = "0"
        
        if cString.characters.count == 6 {
            rString = (cString as NSString).substring(to: 2)
            gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
            bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        } else {
            aString = (cString as NSString).substring(to: 2)
            rString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
            gString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
            bString = ((cString as NSString).substring(from: 6) as NSString).substring(to: 2)
        }
        
        var a:CUnsignedInt = 0, r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0
        Scanner(string: aString).scanHexInt32(&a)
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(a))
    }
    
    convenience init(hex: String) {
        let color = UIColor.color(withHex: hex)
        self.init(cgColor: color.cgColor)
    }
}
