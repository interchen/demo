//
//  GuideDialogViewController.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/11/20.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit

class GuideDialogViewController: UIViewController {
    
    @IBOutlet weak var simpleButton: UIButton!
    @IBOutlet weak var buttonInView: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let guideDialog = GuideDialog(from: self,
                                      for: self.buttonInView,
                                      with: "hello simple,hello simple,hello simple,hello simple")
        guideDialog.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
