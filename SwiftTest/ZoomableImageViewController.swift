//
//  ZoomableImageViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/29.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit
import SDWebImage
import MBProgressHUD

class ZoomableImageViewController: UIViewController, ICZoomableImageViewDelegate {
    @IBOutlet weak var zoomableImageView: ICZoomableImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        SDImageCache.shared().clearMemory()
//        SDImageCache.shared().clearDisk()
        
        self.zoomableImageView.imageWidthMode = .original
        self.zoomableImageView.load(URL(string: "http://dummyimage.com/500x1000/93c754&text=Linda")!, with: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func zoomableImageView(_ icZoomableImageView: ICZoomableImageView, load url: URL, into imageView: UIImageView) {
        
        let hud = MBProgressHUD.showAdded(to: imageView, animated: true)
        hud.mode = .annularDeterminate
        hud.progress = 0.0
//        imageView.sd_setImage(with: url, placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), progress: { (receivedSize, expectedSize) in
//            let percent = receivedSize / expectedSize
//            hud.progress = Float(percent)
//        }) { (image, error, type, imageURL) in
//            hud.hide(animated: true)
//        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
