//
//  CustomNavigationController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/26.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

// refer from: http://www.jianshu.com/p/25505f48e0fd
var alphaViewKey = "alphaViewKey"
extension UINavigationController {
    var zj_alphaView: UIView? {
        set {
            objc_setAssociatedObject(self, &alphaViewKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        
        get {
            return objc_getAssociatedObject(self, &alphaViewKey) as? UIView
        }
    }

    func zj_updateWithAlpha(alpha: CGFloat) {
//        if zj_alphaView == nil {
//            zj_alphaView = UIView(frame: CGRect(x: 0.0, y: -20.0, width: UIScreen.main.bounds.width, height: 64.0))
//            zj_alphaView?.isUserInteractionEnabled = false
//            navigationBar.setBackgroundImage(UIImage(), for: .default)
//            navigationBar.shadowImage = UIImage()
//            navigationBar.insertSubview(zj_alphaView!, at: 0)
//        }
//        navigationBar.sendSubview(toBack: zj_alphaView!)
//        zj_alphaView?.backgroundColor = UIColor.init(patternImage: #imageLiteral(resourceName: "nav-bg-default")).withAlphaComponent(alpha)
    }
}

class CustomNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var childViewControllerForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
