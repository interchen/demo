//
//  StokeButton.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 16/8/23.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class StokeButton: UIButton {
    
    @IBInspectable var insetHorizontal: CGFloat = 8.0 {
        didSet {
            setup()
        }
    }
    
    @IBInspectable var insetVertical: CGFloat = 12.0 {
        didSet {
            setup()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    fileprivate func setup() {
        self.layer.borderColor = self.titleColor(for: UIControlState())?.cgColor
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = 4.0
        self.layer.masksToBounds = true
        self.contentEdgeInsets = UIEdgeInsetsMake(insetVertical, insetHorizontal, insetVertical, insetHorizontal)
    }
}

class HKFillButton: UIButton {
    
    fileprivate func imageWithColor(_ color: UIColor) -> UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(color.cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
    }
    
    @IBInspectable var myCornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = myCornerRadius
        }
    }
    
    @IBInspectable var normalBGColor: UIColor = UIColor.clear {
        didSet {
            self.setBackgroundImage(imageWithColor(normalBGColor), for: UIControlState())
        }
    }
    
    @IBInspectable var disabledBGColor: UIColor = UIColor.clear {
        didSet {
            self.setBackgroundImage(imageWithColor(disabledBGColor), for: .disabled)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = myCornerRadius
        self.layer.masksToBounds = true
    }
    
}
