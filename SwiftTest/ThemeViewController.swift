//
//  ThemeViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/27.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class ThemeViewController: UIViewController {
    @IBOutlet weak var myLabel: MyLabel!
    @IBOutlet weak var myButton: MyButton!
    
    var theme = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func handleChangeButton(_ sender: Any) {
        theme = !theme
        if theme {
            updateTheme(color: .red)
        } else {
            updateTheme(color: .black)
        }
    }
    
    func updateTheme(color: UIColor) {
        for subview in self.view.subviews {
            if let themeView = subview as? ThemeProtocol {
                themeView.themeDidChanged(color: color)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
