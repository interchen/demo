//
//  ConstraintViewController.swift
//  SwiftTest
//
//  Created by 陈颜俊 on 2017/9/1.
//  Copyright © 2017年 陈 颜俊. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON

class ConstraintViewController: UIViewController {
    
    @IBOutlet weak var webview: WKWebView!
    
    let baseURL = "http://backstage.homeking365.com/info/orderForGs?id="
    var index = 0
    var fired = false
    var orderIds: [JSON]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let json = jsonData(from: "orderIds")
        orderIds = json!["orderIds"].arrayValue
        
        webview.navigationDelegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.title = "\(index)/\(orderIds!.count)"
        loadNext()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func jsonData(from fileName: String) -> JSON? {
        do {
            let jsonData = try Data(contentsOf: Bundle.main.url(forResource: fileName, withExtension: "json")!)
            return try JSON(data: jsonData)
        } catch {
            return nil
        }
    }
    
    @IBAction func handleSaveButton(_ sender: Any) {
        self.fired = true
        self.loadNext()
    }
    
    @objc func loadNext() -> Void {
        guard let orderIds = self.orderIds else {
            return
        }
        
        guard index < orderIds.count else { return }
        
        let orderId = orderIds[index].intValue
        let url = self.baseURL + String(orderId)
        self.webview.load(URLRequest(url: URL(string: url)!, cachePolicy:.useProtocolCachePolicy, timeoutInterval: 86400))
    }
    
    @objc func screenshot(_ orderId: String) -> Void {
        var img: UIImage
        UIGraphicsBeginImageContextWithOptions(webview.scrollView.frame.size, webview.scrollView.isOpaque, 0.0)
        
//        let savedContentOffset = webview.scrollView.contentOffset
//        let savedFrame = webview.scrollView.frame

//        webview.scrollView.contentOffset = .zero
//        webview.scrollView.frame = CGRect(x: 0.0, y: 0.0, width: webview.scrollView.contentSize.width, height: webview.scrollView.contentSize.height)
        webview.scrollView.layer.render(in: UIGraphicsGetCurrentContext()!)
        img = UIGraphicsGetImageFromCurrentImageContext()!
        
//        webview.scrollView.contentOffset = savedContentOffset
//        webview.scrollView.frame = savedFrame
        
        UIGraphicsEndImageContext()
        
//        UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil)
        saveImageToDocument(img, orderId: orderId)
    }
    
    func saveImageToDocument(_ img: UIImage, orderId: String) -> Void {
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
//        print("docpath = \(documentsDirectoryURL)")
//        print("saveImageToDocument: \(orderId)")
        
        // create a name for your image
        let fileURL = documentsDirectoryURL.appendingPathComponent("sz-chutiyan-8-\(self.index).jpg")
        try! UIImageJPEGRepresentation(img, 1.0)?.write(to: fileURL, options: .atomic)
        
        self.index += 1
        self.title = "\(self.index)/\(self.orderIds!.count)"
        self.loadNext()
    }
    
    func setupView() {
        var buttons: [UIButton] = []
        for index in 0...5 {
            let button = UIButton(frame: .zero)
            button.setTitleColor(.black, for: .normal)
            button.setTitle("button \(index)", for: .normal)
            button.layer.borderColor = UIColor.gray.cgColor
            button.layer.borderWidth = 1.0
            button.translatesAutoresizingMaskIntoConstraints = false
            self.view.addSubview(button)
            buttons.append(button)
        }
        
        for index in 0...5 {
            let button = buttons[index]
            if index == 0 {
                button.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8.0).isActive = true
                button.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20.0).isActive = true
            } else {
                let preButton = buttons[index - 1]
                button.leadingAnchor.constraint(equalTo: preButton.trailingAnchor, constant: 8.0).isActive = true
                button.topAnchor.constraint(equalTo: preButton.topAnchor, constant: 0.0).isActive = true
            }
        }
    }
}

extension ConstraintViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        guard self.fired else { return }
        
        let components = URLComponents(string: webView.url!.absoluteString)
        let orderId = components?.queryItems?.first?.value ?? ""
        
//        print("didFinish: \(orderId)")
        self.screenshot(orderId)
    }
}
