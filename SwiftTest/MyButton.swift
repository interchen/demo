//
//  MyButton.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/27.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class MyButton: UIButton, ThemeProtocol {
    func themeDidChanged(color: UIColor) {
        self.setTitleColor(color, for: .normal)
    }
}
