//
//  StatusBarTestViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/19.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class StatusBarTestViewController: UIViewController {
    
    var shouldHideStatusBar = false

    override func viewDidLoad() {
        modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        modalTransitionStyle = .crossDissolve
        modalPresentationCapturesStatusBarAppearance = true
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.shouldHideStatusBar = true
        self.setNeedsStatusBarAppearanceUpdate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return self.shouldHideStatusBar
    }
    
    @IBAction func handleCloseButton(_ sender: Any) {
        self.shouldHideStatusBar = false
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
