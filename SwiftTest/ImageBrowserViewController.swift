//
//  ImageBrowserViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/26.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class ImageBrowserViewController: UIViewController {
    
    var shouldHideStatusBar = false

    override func viewDidLoad() {
        super.viewDidLoad()

        self.modalPresentationCapturesStatusBarAppearance = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.shouldHideStatusBar = true
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var prefersStatusBarHidden: Bool {
        return self.shouldHideStatusBar
    }
    
    @IBAction func handleDismiss(_ sender: Any) {
        self.shouldHideStatusBar = false
        self.setNeedsStatusBarAppearanceUpdate()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
