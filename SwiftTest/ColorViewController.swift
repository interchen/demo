//
//  ColorViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/11/7.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit

class SubSegmentedControl: UISegmentedControl {
    
}

class SubButton: UIButton {
    
}

class ColorViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        SubButton.appearance().tintColor = .green
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func handleAppearenceButton(_ sender: Any) {
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
