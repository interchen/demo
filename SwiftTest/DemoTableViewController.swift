//
//  DemoTableViewController.swift
//  SwiftTest
//
//  Created by 陈 颜俊 on 2016/12/22.
//  Copyright © 2016年 陈 颜俊. All rights reserved.
//

import UIKit
import FDFullscreenPopGesture
import StoreKit
import SwiftyRSA

class DemoTableViewController: UITableViewController {
    
    var feedback: Any?

    override func viewDidLoad() {
        super.viewDidLoad()

//        self.navigationController?.navigationBar.barTintColor = .blue
        if #available(iOS 10.0, *) {
            self.tabBarController?.delegate = self
            feedback = UISelectionFeedbackGenerator()
            let _feedback = feedback as! UISelectionFeedbackGenerator
            _feedback.prepare()
        }
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(named: "nav-bg-a6"), for: .default)
//        self.navigationController?.zj_updateWithAlpha(alpha: 1)
        
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
        }
        
        do {
            let publicKey = try PublicKey(pemNamed: "rsa_public_key")
            let clear = try ClearMessage(string: "hello world", using: .utf8)
            let encrypted = try clear.encrypted(with: publicKey, padding: .PKCS1)
            let base64String = encrypted.base64String
            print("encrypted = " + base64String)
            /* 
             fYdsQVyqI+E33IFEFXUfruNRY7cv3WHeuJKVdisWm+16lRiEKKjOF57HbhnDjTVEeULKCbIiebZw
             zcI2l+iAFWoNZqJSFK9hrRF52aLgs67iz9XEiBcm/SgXASwLeSi+OsYpUwr6WqYtkpUuNmXB9U7f
             iQcU+56KKRClq2GfCIw=
            */
            
            let privateKey = try PrivateKey(pemNamed: "pkcs8_rsa_private_key")
            let encryptedMessage = try EncryptedMessage(base64Encoded: base64String)
            let decrypted = try encryptedMessage.decrypted(with: privateKey, padding: .PKCS1)
            let decryptedString = try decrypted.string(encoding: .utf8)
            print("encrypted decrypted = " + decryptedString)
        } catch {
            print("encrypted error = " + error.localizedDescription)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else {
            return
        }
        
        if cell.reuseIdentifier == "twicePushCell" {
            let testViewController = TestViewController.init(nibName: "TestViewController", bundle: nil)
            self.navigationController?.pushViewController(testViewController, animated: true)
        }
    }
}

extension DemoTableViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if #available(iOS 10.0, *) {
            let _feedback = feedback as! UISelectionFeedbackGenerator
            _feedback.selectionChanged()
        }
    }
}
